// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bz_record.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BzRecord> _$bzRecordSerializer = new _$BzRecordSerializer();

class _$BzRecordSerializer implements StructuredSerializer<BzRecord> {
  @override
  final Iterable<Type> types = const [BzRecord, _$BzRecord];
  @override
  final String wireName = 'BzRecord';

  @override
  Iterable<Object> serialize(Serializers serializers, BzRecord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    Object value;
    value = object.name;
    if (value != null) {
      result
        ..add('NAME')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.series;
    if (value != null) {
      result
        ..add('SERIES')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.symbol;
    if (value != null) {
      result
        ..add('SYMBOL')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.reference;
    if (value != null) {
      result
        ..add('Document__Reference__Field')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType(Object)])));
    }
    return result;
  }

  @override
  BzRecord deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BzRecordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'NAME':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SERIES':
          result.series = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SYMBOL':
          result.symbol = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'Document__Reference__Field':
          result.reference = serializers.deserialize(value,
                  specifiedType: const FullType(
                      DocumentReference, const [const FullType(Object)]))
              as DocumentReference<Object>;
          break;
      }
    }

    return result.build();
  }
}

class _$BzRecord extends BzRecord {
  @override
  final String name;
  @override
  final String series;
  @override
  final String symbol;
  @override
  final DocumentReference<Object> reference;

  factory _$BzRecord([void Function(BzRecordBuilder) updates]) =>
      (new BzRecordBuilder()..update(updates)).build();

  _$BzRecord._({this.name, this.series, this.symbol, this.reference})
      : super._();

  @override
  BzRecord rebuild(void Function(BzRecordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BzRecordBuilder toBuilder() => new BzRecordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BzRecord &&
        name == other.name &&
        series == other.series &&
        symbol == other.symbol &&
        reference == other.reference;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), series.hashCode), symbol.hashCode),
        reference.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BzRecord')
          ..add('name', name)
          ..add('series', series)
          ..add('symbol', symbol)
          ..add('reference', reference))
        .toString();
  }
}

class BzRecordBuilder implements Builder<BzRecord, BzRecordBuilder> {
  _$BzRecord _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _series;
  String get series => _$this._series;
  set series(String series) => _$this._series = series;

  String _symbol;
  String get symbol => _$this._symbol;
  set symbol(String symbol) => _$this._symbol = symbol;

  DocumentReference<Object> _reference;
  DocumentReference<Object> get reference => _$this._reference;
  set reference(DocumentReference<Object> reference) =>
      _$this._reference = reference;

  BzRecordBuilder() {
    BzRecord._initializeBuilder(this);
  }

  BzRecordBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _series = $v.series;
      _symbol = $v.symbol;
      _reference = $v.reference;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BzRecord other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BzRecord;
  }

  @override
  void update(void Function(BzRecordBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BzRecord build() {
    final _$result = _$v ??
        new _$BzRecord._(
            name: name, series: series, symbol: symbol, reference: reference);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
